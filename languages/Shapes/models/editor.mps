<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d273c9d3-a988-45a6-8781-45f756a10d3e(Shapes.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="wo3v" ref="r:e2fa0125-7dac-434e-bdfb-7149acee0229(Shapes.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="5enpFrv_fjE">
    <ref role="1XX52x" to="wo3v:5enpFrv_eUP" resolve="Circle" />
    <node concept="3EZMnI" id="5enpFrv_qYy" role="2wV5jI">
      <node concept="3F0ifn" id="5enpFrv_qYK" role="3EZMnx">
        <property role="3F0ifm" value="circle" />
      </node>
      <node concept="l2Vlx" id="5enpFrv_qY_" role="2iSdaV" />
      <node concept="3F0ifn" id="5enpFrv_qZ9" role="3EZMnx">
        <property role="3F0ifm" value="x: " />
      </node>
      <node concept="3F0A7n" id="5enpFrv_qZm" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eUQ" resolve="x" />
      </node>
      <node concept="3F0ifn" id="5enpFrv_qZB" role="3EZMnx">
        <property role="3F0ifm" value="y:" />
      </node>
      <node concept="3F0A7n" id="5enpFrv_qZW" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eUS" resolve="y" />
      </node>
      <node concept="3F0ifn" id="5enpFrv_r0l" role="3EZMnx">
        <property role="3F0ifm" value="radius:" />
      </node>
      <node concept="3F0A7n" id="5enpFrv_r0M" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eUV" resolve="radius" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5enpFrv_fkk">
    <ref role="1XX52x" to="wo3v:5enpFrv_eUZ" resolve="Square" />
    <node concept="3EZMnI" id="5enpFrv_tJf" role="2wV5jI">
      <node concept="l2Vlx" id="5enpFrv_tJg" role="2iSdaV" />
      <node concept="3F0ifn" id="5enpFrv_tJo" role="3EZMnx">
        <property role="3F0ifm" value="square" />
      </node>
      <node concept="3F0ifn" id="5enpFrv_tJw" role="3EZMnx">
        <property role="3F0ifm" value="x:" />
      </node>
      <node concept="3F0A7n" id="5enpFrv_tJH" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eV0" resolve="upperLeftX" />
      </node>
      <node concept="3F0ifn" id="5enpFrv_tJY" role="3EZMnx">
        <property role="3F0ifm" value="y:" />
      </node>
      <node concept="3F0A7n" id="5enpFrv_tKj" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eV2" resolve="upperLeftY" />
      </node>
      <node concept="3F0ifn" id="5enpFrv_tKG" role="3EZMnx">
        <property role="3F0ifm" value="size:" />
      </node>
      <node concept="3F0A7n" id="5enpFrv_tL9" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eV5" resolve="size" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5enpFrv_meQ">
    <ref role="1XX52x" to="wo3v:5enpFrv_eV9" resolve="Canvas" />
    <node concept="3EZMnI" id="5enpFrv_mEG" role="2wV5jI">
      <node concept="l2Vlx" id="5enpFrv_mEH" role="2iSdaV" />
      <node concept="3F0ifn" id="5enpFrv_mET" role="3EZMnx">
        <property role="3F0ifm" value="Painting " />
      </node>
      <node concept="3F0A7n" id="5enpFrv_mEZ" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F2HdR" id="5enpFrv_mFq" role="3EZMnx">
        <ref role="1NtTu8" to="wo3v:5enpFrv_eVc" resolve="shapes" />
        <node concept="2iRkQZ" id="5enpFrv_mFt" role="2czzBx" />
        <node concept="VPM3Z" id="5enpFrv_mFu" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="pVoyu" id="5enpFrv_mF$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
</model>

