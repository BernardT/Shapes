<?xml version="1.0" encoding="UTF-8"?>
<solution name="Shapes.sandbox" uuid="0a160e21-bf80-47ee-8f01-a4dea7bc49cd" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:1e8a3103-c8bf-462f-bbf6-6e948cc5dd13:Shapes" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="0a160e21-bf80-47ee-8f01-a4dea7bc49cd(Shapes.sandbox)" version="0" />
  </dependencyVersions>
</solution>

