<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:90029b6f-e733-431b-94d4-147d01947a96(Shapes.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="1e8a3103-c8bf-462f-bbf6-6e948cc5dd13" name="Shapes" version="0" />
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="1" />
  </languages>
  <imports />
  <registry>
    <language id="1e8a3103-c8bf-462f-bbf6-6e948cc5dd13" name="Shapes">
      <concept id="6023395962252488373" name="Shapes.structure.Circle" flags="ng" index="2c0Sw3">
        <property id="6023395962252488374" name="x" index="2c0Sw0" />
        <property id="6023395962252488379" name="radius" index="2c0Swd" />
        <property id="6023395962252488376" name="y" index="2c0Swe" />
      </concept>
      <concept id="6023395962252488383" name="Shapes.structure.Square" flags="ng" index="2c0Sw9">
        <property id="6023395962252488389" name="size" index="2c0SxN" />
        <property id="6023395962252488386" name="upperLeftY" index="2c0SxO" />
        <property id="6023395962252488384" name="upperLeftX" index="2c0SxQ" />
      </concept>
      <concept id="6023395962252488393" name="Shapes.structure.Canvas" flags="ng" index="2c0SxZ">
        <child id="6023395962252488396" name="shapes" index="2c0SxU" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2c0SxZ" id="5enpFrv_fj7">
    <property role="TrG5h" value="FirstDraw" />
    <node concept="2c0Sw3" id="5enpFrv_fj8" role="2c0SxU">
      <property role="2c0Sw0" value="20" />
      <property role="2c0Swe" value="20" />
      <property role="2c0Swd" value="10" />
    </node>
    <node concept="2c0Sw9" id="5enpFrv_fjd" role="2c0SxU">
      <property role="2c0SxQ" value="30" />
      <property role="2c0SxO" value="30" />
      <property role="2c0SxN" value="20" />
    </node>
  </node>
  <node concept="2c0SxZ" id="5enpFrv_tGN">
    <property role="TrG5h" value="TropBo" />
    <node concept="2c0Sw3" id="5enpFrv_tHf" role="2c0SxU">
      <property role="2c0Sw0" value="55" />
      <property role="2c0Swe" value="65" />
      <property role="2c0Swd" value="25" />
    </node>
    <node concept="2c0Sw9" id="5enpFrv_xT3" role="2c0SxU">
      <property role="2c0SxQ" value="15" />
      <property role="2c0SxO" value="25" />
      <property role="2c0SxN" value="25" />
    </node>
  </node>
</model>

